package Ro.Orange;

public class SimpleThreadTest {

    public static void main(String[] args) {
        Thread S1 = new SimpleThread("Pleasent Gardens Hotel");
        S1.start();
        Thread S2 = new SimpleThread("Daydream Hotel");
        S2.start();
        Thread S3 = new SimpleThread("Royal Arc Resort & Spa");
        S3.start();
    }
}
